import { it } from "mocha";
import { sum, multipy} from "../src/calc";
import { expect } from "chai";

describe("calc tests", () => {
    context("sum tests", () => {
        it("check type of sum", () => {
            expect(sum).to.be.a("function");
        });
        it("check sum output for 2 numbers", () => {
            expect(sum(2, 3)).to.equal(5);
        });
        it("check sum output for 5 numbers", () => {
            expect(sum(1,2,3,4,5)).to.equal(15);
        });
    });

    context("multipy tests", () => {
        const delay = (ms:number)=> new Promise((resolve)=> setTimeout(resolve,ms))

        it("check type of multipy", () => {
            expect(sum).to.be.a("function");
        });
        it("check multipy output", () => {
            expect(multipy(2,2,3,4,5)).to.eql([4,6,8,10]);
        });
        it("check multipy with delay", async () => {
            await delay(1500)
            expect(multipy(2,2,3,4,5)).to.eql([4,6,8,10]);
        });
    });
    
});