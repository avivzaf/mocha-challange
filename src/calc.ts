export function sum(...nums: number[]) {
    return nums.reduce((acc, num) => acc + num);
}

export function multipy(multiplier: number, ...nums: number[]) {
    return nums.map((num) => multiplier*num);
}
